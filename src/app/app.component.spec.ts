import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { Robot } from './toy-robot/robot';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        FormsModule
        ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Angular 5 Toy Robot Simulation'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Angular 5 Toy Robot Simulation');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to the Angular 5 Toy Robot Simulation!');
  }));

  it('should be able to execute Robot commands from the UI', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    const spyOnExecuteCommands = spyOn(app, 'executeCommands');
    const compiled = fixture.debugElement.nativeElement;

    compiled.querySelector('.btnExecute').click();
    expect(spyOnExecuteCommands).toHaveBeenCalled();
  }));

  it('should be able to execute multiple Robot commands', async(() => {
    const spyOnRobotRunCommand = spyOn(Robot.prototype, 'runCommand');
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;

    app.commands.text = 'PLACE 0,0,NORTH\nMOVE\nLEFT\nREPORT';
    app.executeCommands();
    expect(spyOnRobotRunCommand).toHaveBeenCalledTimes(4);
  }));

  it('should not display results if the response of REPORT command is empty', async(() => {
    spyOn(Robot.prototype, 'runCommand').and.callFake((x) => {
      if (x === 'REPORT') {
        return;
      }
    });
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;

    const compiled = fixture.debugElement.nativeElement;

    app.commands.text = 'REPORT';
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      compiled.querySelector('.btnExecute').click();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(compiled.querySelector('#resultTxt').value).toBe('');
      });
    });
  }));
});
