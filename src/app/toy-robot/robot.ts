import { RobotCommands } from './robot-commands.enum';
import { RobotDirection } from './robot-direction.enum';

export class Robot {
  private readonly maxPositionX: number;
  private readonly maxPositionY: number;
  private readonly minPositionX: number;
  private readonly minPositionY: number;
  private readonly validDirections: string[];

  private currentPositionX: number;
  private currentPositionY: number;
  private currentFaceDirection: RobotDirection;
  private robotNotYetOnTableText: String = 'Robot not yet Placed on the table';
  private dropFromTableError: String = 'Error: Command ' + RobotCommands.Move + ' would make robot drop from the table';
  private invalidCommandText: String = 'Error: invalid Command "{0}"';
  private invalidPlaceCommandText: String = 'Error: invalid ' + RobotCommands.Place + ' Command arguments "{0}"';
  private invalidPlaceCommandArgument = 'Error: invalid ' + RobotCommands.Place + ' Command argument {0} = "{1}"';
  private invalidPlaceCoordinate = 'Error: ' + RobotCommands.Place + ' Command argument {0} = "{1}" is not in the table';

  constructor(originX: number, originY: number, maxX: number, maxY) {
    this.minPositionX = originX;
    this.minPositionY = originY;
    this.maxPositionX = maxX;
    this.maxPositionY = maxY;

    this.validDirections = [
      RobotDirection.East,
      RobotDirection.North,
      RobotDirection.West,
      RobotDirection.South
    ];
  }

  public runCommand(command: string): string {
    const commandParts = command.split(' ');
    const invalidCommandText = this.invalidCommandText.replace('{0}', command);

    switch (commandParts[0]) {
      case RobotCommands.Place:
        if (commandParts.length !== 2) {
          console.error(invalidCommandText);
        }
        this.CommandPlace(commandParts);
        break;
      case RobotCommands.Move:
        if (commandParts.length > 1) {
          console.error(invalidCommandText);
          return;
        }
        this.CommandMove();
        break;
      case RobotCommands.Left:
        if (commandParts.length > 1) {
          console.error(invalidCommandText);
          return;
        }
        this.CommandLeft();
        break;
      case RobotCommands.Right:
        if (commandParts.length > 1) {
          console.error(invalidCommandText);
          return;
        }
        this.CommandRight();
        break;
      case RobotCommands.Report:
        if (commandParts.length > 1) {
          console.error(invalidCommandText);
          return;
        }
        return this.CommandReport();
      default:
        console.error(this.invalidCommandText.replace('{0}', commandParts[0]));
    }
  }

  private IsRobotOnTable() {
    return this.currentPositionX === 0 || !!this.currentPositionX;
  }

  private CommandReport() {
    if (!this.IsRobotOnTable()) {
      console.error(this.robotNotYetOnTableText);
      return;
    }

    return String(this.currentPositionX) + ',' + String(this.currentPositionY) + ',' + this.currentFaceDirection;
  }

  private CommandRight(): string {
    if (!this.IsRobotOnTable()) {
      console.error(this.robotNotYetOnTableText);
      return;
    }

    switch (this.currentFaceDirection) {
      case RobotDirection.East:
        this.currentFaceDirection = RobotDirection.South;
        break;
      case RobotDirection.South:
        this.currentFaceDirection = RobotDirection.West;
        break;
      case RobotDirection.West:
        this.currentFaceDirection = RobotDirection.North;
        break;
      case RobotDirection.North:
        this.currentFaceDirection = RobotDirection.East;
        break;
    }
    return null;
  }

  private CommandLeft(): void {
    if (!this.IsRobotOnTable()) {
      console.error(this.robotNotYetOnTableText);
      return;
    }

    switch (this.currentFaceDirection) {
      case RobotDirection.East:
        this.currentFaceDirection = RobotDirection.North;
        break;
      case RobotDirection.South:
        this.currentFaceDirection = RobotDirection.East;
        break;
      case RobotDirection.West:
        this.currentFaceDirection = RobotDirection.South;
        break;
      case RobotDirection.North:
        this.currentFaceDirection = RobotDirection.West;
        break;
    }
  }

  private CommandMove(): void {
    if (!this.IsRobotOnTable()) {
      console.error(this.robotNotYetOnTableText);
      return;
    }

    switch (this.currentFaceDirection) {
      case RobotDirection.East:
        if (this.currentPositionX + 1 > this.maxPositionX) {
          console.error(this.dropFromTableError);
          return;
        }
        this.currentPositionX += 1;
        break;
      case RobotDirection.South:
        if (this.currentPositionY - 1 < this.minPositionY) {
          console.error(this.dropFromTableError);
          return;
        }
        this.currentPositionY -= 1;
        break;
      case RobotDirection.West:
        if (this.currentPositionX - 1 < this.minPositionX) {
          console.error(this.dropFromTableError);
          return;
        }
        this.currentPositionX -= 1;
        break;
      case RobotDirection.North:
        if (this.currentPositionY + 1 > this.maxPositionY) {
          console.error(this.dropFromTableError);
          return;
        }
        this.currentPositionY += 1;
        break;
    }
  }

  private CommandPlace(commandParts: string[]): void {
    const placedPositionParts = commandParts[1].split(',');

    if (placedPositionParts.length !== 3) {
      console.error(this.invalidPlaceCommandText.replace('{0}', commandParts[1]));
      return;
    }

    const newPositionX = Number(placedPositionParts[0]);
    const newPositionY = Number(placedPositionParts[1]);

    if (newPositionX % 1 !== 0) {
      console.error(this.invalidPlaceCommandArgument.replace('{0}', 'X').replace('{1}', placedPositionParts[0]));
      return;
    }
    if (newPositionX > this.maxPositionX || newPositionX < this.minPositionX) {
      console.error(this.invalidPlaceCoordinate.replace('{0}', 'X').replace('{1}', placedPositionParts[0]));
      return;
    }

    if (newPositionY % 1 !== 0) {
      console.error(this.invalidPlaceCommandArgument.replace('{0}', 'Y').replace('{1}', placedPositionParts[1]));
      return;
    }
    if (newPositionY > this.maxPositionY || newPositionY < this.minPositionY) {
      console.error(this.invalidPlaceCoordinate.replace('{0}', 'Y').replace('{1}', placedPositionParts[1]));
      return;
    }

    if (this.validDirections.indexOf(placedPositionParts[2]) === -1) {
      console.error(this.invalidPlaceCommandArgument.replace('{0}', 'Direction').replace('{1}', placedPositionParts[2]));
      return;
    }

    const newFaceDirection: RobotDirection  = placedPositionParts[2] as RobotDirection;

    this.currentPositionX = newPositionX;
    this.currentPositionY = newPositionY;
    this.currentFaceDirection = newFaceDirection;
  }
}
