import { async } from '@angular/core/testing';
import { Robot } from './robot';
import { RobotCommands } from './robot-commands.enum';

describe('Toy Robot on a 5x5 Table', () => {
  let robot = null;
  beforeEach(async(() => {
    robot = new Robot(0, 0, 4, 4);
  }));

  describe('PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.', () => {
    const placeCommandArguments = '2,1,WEST';
    const placeCommand = RobotCommands.Place + ' ' + placeCommandArguments;

    beforeEach(async(() => {
      robot.runCommand(placeCommand);
    }));

    it('should ignore command when facing direction is invalid, i.e. "PLACE 3,3,EASTE"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 3,3,EASTE';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command without facing direction, i.e. "PLACE 3,3"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 3,3';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command with invalid facing direction, i.e. "PLACE 3,3, "', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 3,3, ';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command with fractions for position X, i.e. "PLACE 0.3,0.4,EAST"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 0.3,0.4,EAST';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command with fractions for position Y, i.e. "PLACE 3,0.4,EAST"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 3,0.4,EAST';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command when target position is outside the table EAST boundary, i.e. "PLACE 5,4,EAST"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 5,4,EAST';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command when target position is outside the table NORTH boundary, i.e. "PLACE 4,5,EAST"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 4,5,EAST';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));
    it('should ignore command when target position is outside the table WEST boundary, i.e. "PLACE 4,-1,EAST"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 4,-1,EAST';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));
    it('should ignore command when target position is outside the table SOUTH boundary, i.e. "PLACE -1,4,EAST"', async(() => {
      const placeCommand2 = RobotCommands.Place + ' -1,4,EAST';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command when Position X is not numeric i.e. PLACE a,1,NORTH', async(() => {
      const placeCommand2 = RobotCommands.Place + ' a,1,NORTH';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore command when Position Y is not numeric i.e. PLACE 1,b,NORTH', async(() => {
      const placeCommand2 = RobotCommands.Place + ' 1,b,NORTH';
      robot.runCommand(placeCommand2);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));
  });

  describe('The first valid command to the robot is a PLACE command, after that, any sequence of commands may ' +
    'be issued, in any order, including another PLACE command. The application should discard all commands in ' +
    'the sequence until a valid PLACE command has been executed', () => {
    it('should ignore other commands before a PLACE command is issued', async(() => {
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();

      robot.runCommand(RobotCommands.Left);
      expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();

      robot.runCommand(RobotCommands.Right);
      expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();
    }));
    it('should ignore other commands before a PLACE command is issued, and accept commands after PLACE is issued', async(() => {
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();

      robot.runCommand(RobotCommands.Place + ' 1,1,NORTH');
      robot.runCommand(RobotCommands.Move);
      robot.runCommand(RobotCommands.Left);

      expect(robot.runCommand(RobotCommands.Report)).toBe('1,2,WEST');
    }));
  });

  describe('MOVE will move the toy robot one unit forward in the direction it is currently facing. ' +
    'LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot. ' +
    'REPORT will announce the X,Y and F of the robot', () => {
    describe('Robot placed on the table at "3,3,EAST"', () => {
      const placeCommandArguments = '3,3,EAST';
      const placeCommand = RobotCommands.Place + ' ' + placeCommandArguments;

      beforeEach(async(() => {
        robot.runCommand(placeCommand);
      }));

      it('should Rotate to SOUTH on RIGHT command', async(() => {
        robot.runCommand(RobotCommands.Right);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,SOUTH');
      }));

      it('should Rotate to NORTH on LEFT command', async(() => {
        robot.runCommand(RobotCommands.Left);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,NORTH');
      }));

      it('should move to 4,3 on MOVE command', async(() => {
        robot.runCommand(RobotCommands.Move);
        expect(robot.runCommand(RobotCommands.Report)).toBe('4,3,EAST');
      }));
    });

    describe('Robot placed on the table at "3,3,SOUTH"', () => {
      const placeCommandArguments = '3,3,SOUTH';
      const placeCommand = RobotCommands.Place + ' ' + placeCommandArguments;

      beforeEach(async(() => {
        robot.runCommand(placeCommand);
      }));

      it('should Rotate to WEST on RIGHT command', async(() => {
        robot.runCommand(RobotCommands.Right);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,WEST');
      }));

      it('should Rotate to EAST on LEFT command', async(() => {
        robot.runCommand(RobotCommands.Left);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,EAST');
      }));

      it('should move to 3,2 on MOVE command', async(() => {
        robot.runCommand(RobotCommands.Move);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,2,SOUTH');
      }));
    });

    describe('Robot placed on the table at "3,3,WEST"', () => {
      const placeCommandArguments = '3,3,WEST';
      const placeCommand = RobotCommands.Place + ' ' + placeCommandArguments;

      beforeEach(async(() => {
        robot.runCommand(placeCommand);
      }));

      it('should Rotate to NORTH on RIGHT command', async(() => {
        robot.runCommand(RobotCommands.Right);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,NORTH');
      }));

      it('should Rotate to SOUTH on LEFT command', async(() => {
        robot.runCommand(RobotCommands.Left);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,SOUTH');
      }));

      it('should move to 2,3 on MOVE command', async(() => {
        robot.runCommand(RobotCommands.Move);
        expect(robot.runCommand(RobotCommands.Report)).toBe('2,3,WEST');
      }));
    });

    describe('Robot placed on the table at "3,3,NORTH"', () => {
      const placeCommandArguments = '3,3,NORTH';
      const placeCommand = RobotCommands.Place + ' ' + placeCommandArguments;

      beforeEach(async(() => {
        robot.runCommand(placeCommand);
      }));

      it('should Rotate to EAST on RIGHT command', async(() => {
        robot.runCommand(RobotCommands.Right);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,EAST');
      }));

      it('should Rotate to WEST on LEFT command', async(() => {
        robot.runCommand(RobotCommands.Left);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,WEST');
      }));

      it('should move to 3,4 on MOVE command', async(() => {
        robot.runCommand(RobotCommands.Move);
        expect(robot.runCommand(RobotCommands.Report)).toBe('3,4,NORTH');
      }));
    });
  });

  describe('The toy robot must not fall off the table during movement. This also includes the initial placement of the toy robot.', () => {
    it('should ignore command MOVE when facing SOUTH from Position Y = 0', async(() => {
      const placeCommand = RobotCommands.Place + ' 2,0,SOUTH';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBe('2,0,SOUTH');
    }));

    it('should ignore command MOVE when facing NORTH from Position Y = 4', async(() => {
      const placeCommand = RobotCommands.Place + ' 3,4,NORTH';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBe('3,4,NORTH');
    }));

    it('should ignore command MOVE when facing WEST from Position X = 0', async(() => {
      const placeCommand = RobotCommands.Place + ' 0,4,WEST';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBe('0,4,WEST');
    }));

    it('should ignore command MOVE when facing EAST from Position X = 4', async(() => {
      const placeCommand = RobotCommands.Place + ' 4,4,EAST';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBe('4,4,EAST');
    }));

    describe('A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands', () => {
      it('should ignore Command REPORT when not on the table', async(() => {
        expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();
      }));
      it('should ignore command MOVE when not on the table', async(() => {
        robot.runCommand(RobotCommands.Move);
        expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();
      }));
      it('should ignore command LEFT when not on the table', async(() => {
        robot.runCommand(RobotCommands.Left);
        expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();
      }));
      it('should ignore command RIGHT when not on the table', async(() => {
        robot.runCommand(RobotCommands.Right);
        expect(robot.runCommand(RobotCommands.Report)).toBeUndefined();
      }));
    });
    describe('Any movement that would result in the robot falling from the table must be prevented, ' +
      'however further valid movement commands must still be allowed', () => {
      it('should ignore command MOVE when placed on 4,4,NORTH and process further ' +
        'commands after that. i.e. LEFT, LEFT, MOVE should result in 4,3,SOUTH', async(() => {
        robot.runCommand(RobotCommands.Place + ' 4,4,NORTH');
        robot.runCommand(RobotCommands.Move);
        robot.runCommand(RobotCommands.Left);
        robot.runCommand(RobotCommands.Left);
        robot.runCommand(RobotCommands.Move);
        expect(robot.runCommand(RobotCommands.Report)).toBe('4,3,SOUTH');
      }));
    });
  });

  describe('Provided test examples', () => {
    it('should output "0,1,NORTH" for Example a', async(() => {
      const placeCommand = RobotCommands.Place + ' 0,0,NORTH';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Move);
      expect(robot.runCommand(RobotCommands.Report)).toBe('0,1,NORTH');
    }));

    it('should output "0,0,WEST" for Example b', async(() => {
      const placeCommand = RobotCommands.Place + ' 0,0,NORTH';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Left);
      expect(robot.runCommand(RobotCommands.Report)).toBe('0,0,WEST');
    }));

    it('should output "3,3,NORTH" for Example c', async(() => {
      const placeCommand = RobotCommands.Place + ' 1,2,EAST';
      robot.runCommand(placeCommand);
      robot.runCommand(RobotCommands.Move);
      robot.runCommand(RobotCommands.Move);
      robot.runCommand(RobotCommands.Left);
      robot.runCommand(RobotCommands.Move);

      expect(robot.runCommand(RobotCommands.Report)).toBe('3,3,NORTH');
    }));
  });

  describe('Additional command Validations', () => {
    const placeCommandArguments = '2,1,WEST';
    const placeCommand = RobotCommands.Place + ' ' + placeCommandArguments;

    beforeEach(async(() => {
      robot.runCommand(placeCommand);
    }));

    it('should ignore non-existing commands', async(() => {
      robot.runCommand('SHOOT 1,2');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
      robot.runCommand('JUMP');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
      robot.runCommand('FLY');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore MOVE command with other characters, i.e. "MOVE 2"', async(() => {
      robot.runCommand(RobotCommands.Move + ' 2');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore LEFT command with other characters, i.e. "LEFT 2"', async(() => {
      robot.runCommand(RobotCommands.Left + ' 2');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore RIGHT command with other characters, i.e. "RIGHT 2"', async(() => {
      robot.runCommand(RobotCommands.Right + ' 2');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));
    it('should ignore RIGHT command with other characters, i.e. " RIGHT"', async(() => {
      robot.runCommand(' ' + RobotCommands.Right);
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));

    it('should ignore REPORT command with other characters, i.e. "REPORT 2"', async(() => {
      robot.runCommand(RobotCommands.Report + ' 2');
      expect(robot.runCommand(RobotCommands.Report)).toBe(placeCommandArguments);
    }));
  });
});
