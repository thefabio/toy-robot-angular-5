import { Component, OnInit } from '@angular/core';
import { Robot } from './toy-robot/robot';
import { RobotCommands } from './toy-robot/robot-commands.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Angular 5 Toy Robot Simulation';
  btnExecuteText = 'Execute';
  btnResetText = 'Reset';
  private robot: Robot;
  public commands = {
    text: '',
    result: ''
  };
  ngOnInit(): void {
    this.reset();
  }
  executeCommands() {
    const commands = this.commands.text.split('\n');
    this.commands.result = '';

    for (const command of commands) {
      if (command === RobotCommands.Report) {
        const report = this.robot.runCommand(command);
        if (!!report) {
          this.commands.result += report + '\n';
        }

        continue;
      }

      this.robot.runCommand(command);
    }
  }

  reset() {
    this.robot = new Robot(0, 0, 4, 4);
    this.commands = {
      text: 'PLACE 0,1,NORTH\nREPORT',
      result: ''
    };
  }
}
