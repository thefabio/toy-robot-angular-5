ToyRobot
==================================

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.2.

It was modified to respond to the coding challenge described at [ToyRobot.md](ToyRobot.md)

## Setting up
* Verify that you are running at least node 6.9.x and npm 3.x.x by running node -v and npm -v in a terminal/console window

* This code was developed and tested with Google Chrome v 62.0 (64 bits). Because of that, in order to run the test cases, it requires Google Chrome installed.

execute the following to install angular CLI (ng command)
```
npm install -g @angular/cli
```

execute the following to install dependencies locally
```
npm install 
```

## Serving the application

Run `ng serve`

Navigate to `http://localhost:4200/` 

Please make sure to have the browser console window open for added error reporting

## Running unit tests

Run `ng test` to execute the unit tests

## Code coverage report

To generate the coverage report run `ng test --code-coverage`

After that, the coverage report can be accessed from `.\coverage\index.html`
